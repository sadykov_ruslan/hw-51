import React from 'react';

let unsorted =[];
let sorted = [];

const getNumbers = (min,max) =>{

    for (let i=0; i < 5; i++){
        let number = Math.floor(Math.random()*(max-min + 1)) + min;
        if(unsorted.includes(number)){
            let newNumber = Math.floor(Math.random()*(max-min + 1)) + min;
            unsorted.push(newNumber);
        } else {
            unsorted.push(number);
        }
    }
    sorted = unsorted.sort((a,b) => a > b ? 1 : -1);
    return sorted;
};

getNumbers(5,36);
 const newNumbers = () =>{
     window.location.reload();
}

const Numbers = props => {

    return (
        <div className="numbers">
            <div className="btn">
                <button type="button" className="new-btn" onClick={newNumbers} >New Numbers</button>
            </div>
            <div className="numbers-inner">
                <div className="number">{props.number1}</div>
                <div className="number">{props.number2}</div>
                <div className="number">{props.number3}</div>
                <div className="number">{props.number4}</div>
                <div className="number">{props.number5}</div>
            </div>
        </div>
    );
};

export default Numbers;
export {sorted};