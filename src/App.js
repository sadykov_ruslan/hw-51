
import './App.css';
import Numbers from "./Numbers/Numbers";
import {sorted} from "./Numbers/Numbers";
import {Component} from "react";


class App extends Component {

    render() {

        return (

            <>
                <Numbers number1={sorted[0]} number2={sorted[1]} number3={sorted[2]} number4={sorted[3]} number5={sorted[4]} reload="window.location.reload()"/>
            </>
        );
    }
}

export default App;
